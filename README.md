# Distributed Communication Frame
##Configuration before running
### JMS config
The publish/subscribe function is implemented using ActiveMQ. So there are something must be done for programming running.

* modify file named *activemq.xml* and add the label for topics you want to support like this:

	`<destinations>
            <topic name="Finance" physicalName="jms.Finance" />
			<topic name="Military" physicalName="jms.Military" />
			<topic name="Polity" physicalName="jms.Polity" />
			<topic name="Log" physicalName="jms.Log" />
      </destinations>`

* sometimes you need to run activemq service manually, enter the command in terminate or command line:

	`activemq`

* finally, if you run it in the IDE, maybe you need to import external jars, *activemq-all-5.11.1.jar*, which can be acquired from floder *activemq-all-5.11.1* in zip or its official site [here](http://activemq.apache.org/download.html)

## Some warnings
### TCP/IP
All ip address used in socket for *Query* command is default local host *127.0.0.1*. Therefore it will cast exception and crash when you try to use *Query* command with the server and client deployed on different hosts.  
If there is a must for modify ip, please enter the *Config.properties* and *Host.properties*.
###JMS
Unluckily, jms also uses local host address. You can modify it in the file named *jndi.properties*