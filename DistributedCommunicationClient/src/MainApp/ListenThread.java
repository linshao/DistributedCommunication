package MainApp;

import ServiceProvider.*;
import ServiceThread.ListenLogInfo;

public class ListenThread extends Thread {

	public ListenThread() {
		
	}

	@Override
	public void run() {
		super.run();
		
		//listen to subscribed topics
		SubscriptionCenter.sharedInstance().startConnection();
		
		//listen log info of hosts
		ListenLogInfo logInfo = new ListenLogInfo();
		new Thread(logInfo).start();
	}

}
