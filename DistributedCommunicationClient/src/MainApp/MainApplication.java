package MainApp;

import DataCenter.ConfigModifier;

public class MainApplication {

	public static void main(String[] args) {
		
		String name = args[0];
		ConfigModifier.sharedInstance().updateName(name);
		
		MainThread mainThread = new MainThread();
		mainThread.start();
		
		ListenThread listenThread = new ListenThread();
		listenThread.start();
	}

}
