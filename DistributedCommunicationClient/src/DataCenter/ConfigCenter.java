package DataCenter;

import java.util.*;

import DataBasic.MetaConfig;

public class ConfigCenter extends MetaConfig {

	private static ConfigCenter instance;
	
	public synchronized static ConfigCenter sharedInstance() {
		if (instance == null)
			instance = new ConfigCenter();
		
		return instance;
	}

	private ConfigCenter() {
		super();
	}
	
	public HashSet<String> generateSetValueForField(String fieldId) {
		String keyOfField = prop.getProperty("keyOf" + fieldId);
		String number = prop.getProperty("numOf" + fieldId);
		int numOfField = Integer.parseInt(number);
		HashSet<String> fieldValue = createStringSetForTarget(keyOfField, numOfField);
		return fieldValue;
	}
	
	private HashSet<String> createStringSetForTarget(String key, int times) {
		HashSet<String> set = new HashSet<String>();
		
		for (int i=1; i<=times; i++) {
			String keyOfProp = key + String.valueOf(i);
			String value = prop.getProperty(keyOfProp);
			if (value != null)
				set.add(value);
		}
		
		return set;
	}
	
	public Map<String, String> generateMapForField(HashSet<String> keySet) {
		Map<String, String> fieldMap = new HashMap<String, String>();
		for (String key : keySet)
			fieldMap.put(key, prop.getProperty(key));
		return fieldMap;
	}
	
}
