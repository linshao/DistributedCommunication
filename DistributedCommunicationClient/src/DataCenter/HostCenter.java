package DataCenter;

import java.util.*;

import DataBasic.MetaHost;

public class HostCenter extends MetaHost{

	private static HostCenter instance;
	
	private static int numOfHost;
	private final static String keyOfNum = "numOfHost";
	
	private Map<Fields, String> fieldIds;
	
	public enum Fields {
		NAMES, IPS, STATES, PORTS, USES
	}
	
	public static synchronized HostCenter sharedInstance() {
		if (instance == null)
			instance = new HostCenter();
		
		return instance;
	}
	
	private HostCenter() {
		super();
		
		numOfHost = Integer.parseInt(prop.getProperty(keyOfNum));
		fieldIds = initializeFieldKeyMap();
	}

	private static HashMap<Fields, String> initializeFieldKeyMap() {
		return new HashMap<Fields, String>() {
			{
				put(Fields.NAMES, "Name");
				put(Fields.IPS, "Ip");
				put(Fields.STATES, "State");
				put(Fields.PORTS, "Port");
				put(Fields.USES, "Using");
			}
		};
	}
	
	public List<String> generateListValueOfField(Fields field) {
		String fieldId = fieldIds.get(field);
		List<String> valueOfField = new ArrayList<String>();
		
		for (int i=1; i<=numOfHost; i++) {
			String key = fieldId + String.valueOf(i);
			String value = prop.getProperty(key);
			valueOfField.add(value);
		}
		
		return valueOfField;
	}

}
