package DataCenter;

import java.util.HashSet;

import DataBasic.MetaConfig;

public class ConfigAccessor extends MetaConfig{
	
	private static ConfigAccessor instance;

	private ConfigCenter configCenter;
	
	private HashSet<String> subTopics;
	private HashSet<String> topics;
	private HashSet<String> commands;
	private HashSet<String> subCommands;
	private HashSet<String> queryCommands;
	private HashSet<String> queryContents;

	private String name;
	
	public void setName(String name) {
		this.name = name;
	}

	public static synchronized ConfigAccessor sharedInstance() {
		if (instance == null)
			instance = new ConfigAccessor();
		return instance;
	}
	
	protected ConfigAccessor() {
		super();
		configCenter = ConfigCenter.sharedInstance();
	}
	
	public HashSet<String> getCommandSet() {
		if (commands == null) 
			this.commands = configCenter.generateSetValueForField("Commands");
		
		return commands;
	}
	
	public HashSet<String> getTopicNameSet() {
		if (topics == null) 
			this.topics = configCenter.generateSetValueForField("Topics");
		
		return topics;
	}
	
	public HashSet<String> getSubscribeCommandSet() {
		if (subCommands == null) 
			this.subCommands = configCenter.generateSetValueForField("SubCommands");
		return subCommands;
	}
	
	public HashSet<String> getSubscribedTopicNamesSet() {
		if (subTopics == null)
			this.subTopics = configCenter.generateSetValueForField("SubTopics");
		
		return subTopics;
	}
	
	public HashSet<String> getQueryCommands() {
		if (queryCommands == null)
			this.queryCommands = configCenter.generateSetValueForField("QueryCommands");
		
		return queryCommands;
	}
	
	public HashSet<String> getQueryContents() {
		if (queryContents == null)
			this.queryContents = configCenter.generateSetValueForField("QueryContents");
		
		return queryContents;
	}
	
	public String getHostName() {
		if (name == null)
			name = prop.getProperty("Name");
		return name;
	}
	
	/*
	 * print hold data
	 */
	
	public void printSubscribedTopicNames() {
		String SubTopics = "Subscribed Topics: ";
		for (String topic : subTopics)
			SubTopics = SubTopics + topic + " ";
		System.out.println(SubTopics);
	}
	
}