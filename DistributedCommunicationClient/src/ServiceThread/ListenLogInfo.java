package ServiceThread;

import DataCenter.ConfigAccessor;
import DataCenter.HostModifier;
import Log.*;

public class ListenLogInfo implements Runnable {

	private static final String TOPIC_NAME = "Log";
	private LogMesListener logListener;
	private LogMesReceiver logReceiver;

	public ListenLogInfo() {
		ConfigAccessor configAccessor = ConfigAccessor.sharedInstance();
		String hostName = configAccessor.getHostName();
		
		logListener = new LogMesListener(TOPIC_NAME, hostName);
		HostInfoModifier hiModifier = HostModifier.sharedInstance();
		logReceiver = new LogMesReceiver(hiModifier);
	}

	@Override
	public void run() {
		logListener.setMessageReceiver(logReceiver);
		logListener.startConnection();
	}

}
