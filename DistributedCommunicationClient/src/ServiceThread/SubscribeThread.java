package ServiceThread;

import ServiceProvider.*;

import java.util.*;

import Service.ServiceThread;

public class SubscribeThread extends ServiceThread {
	
	private SubCmdOperator subCmdOperator;
	
	public SubscribeThread() {
		super();
		
		INPUTHINT = "Please enter the operation and topic names Separated by space:";
		CMDERRORHINT = "Error Command.";
		TAG = "Subscribe has been exited.";
		
		this.subCmdOperator = new SubCmdOperator();
		this.cmdParser = new SubCmdParser();
	}

	@Override
	protected void handleBussinessCommand(String header, List<String> content) {
		super.handleBussinessCommand(header, content);
		SubCmdParser subCmdParser = (SubCmdParser) cmdParser;
		List<String> topics = subCmdParser.parseContentForTopics(content);
		subCmdOperator.operateTheTopics(header, topics);
	}
	
}
