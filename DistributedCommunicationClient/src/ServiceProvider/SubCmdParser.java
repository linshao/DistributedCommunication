package ServiceProvider;

import java.util.*;

import DataCenter.ConfigAccessor;
import Service.CmdParser;

public class SubCmdParser extends CmdParser{

	private static String ERRORTOPIC_HINT;
	
	private HashSet<String> topicNameSet;
	
	public SubCmdParser() {
		
		ERRORTOPIC_HINT = "Error Topic Name: ";
		
		ConfigAccessor configAccessor = ConfigAccessor.sharedInstance();
		this.orglCmdSet = configAccessor.getSubscribeCommandSet();
		this.topicNameSet = configAccessor.getTopicNameSet();
		
		this.exCmdSet = createExtendCmdSet(orglCmdSet);
	}
	
	/*
	 * Parse Input Content
	 */
	
	public List<String> parseContentForTopics(List<String> content) {
		for (int i=0; i< content.size(); i++) {
			String input = content.get(i);
			if (isTheInputNotExitingTopicName(input)) {
				content.remove(input);
				i--; //fix the problem that count change caused by the remove of list
				System.out.println(ERRORTOPIC_HINT + input);
			}
		}
		
		return content;
	}
	
	private boolean isTheInputNotExitingTopicName(String input) {
		if (topicNameSet.contains(input))
			return false;
		else
			return true;
	}

}
