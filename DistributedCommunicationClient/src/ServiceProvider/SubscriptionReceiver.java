package ServiceProvider;

import javax.jms.*;

public class SubscriptionReceiver implements MessageListener{

	public SubscriptionReceiver() {
	}

	@Override
	public void onMessage(Message message) {
		try {
			TextMessage aMessage = (TextMessage) message;
			System.out.println(aMessage.getText());
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}

}
