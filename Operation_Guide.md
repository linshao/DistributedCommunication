#DistributedCommunication
## DCServer
### The command list supported by MainThread
* Publish
* Broadcast
* Query
* Exit

***
MainThread only supports command inputing. If anything else, print "Error Command"
***
### The command list supported by Broadcast
* Exit

***
when you enter something **except** command, it will be broadcasted to other hosts wo join the broadcast group.
***
### The command list supported by Publish
* Finance
* Military
* Polity
* Exit

***
Three foregoing items are the supported topics. It is impossible for current version to *create new topic* not through programming. 
The topic and content would be published must be separated by **single space**.

`Finance somethingwillbepublished`
***
### The command list supported by Query
* Query
* LeaveWords (unimplements)

### The contents can be queried
* Topics
* HostNetwork

***
The whole command for query should at least consists of three parts. They are command of query, target **hosts'** name and query content, separated by single space. As emphasized before, counts of host can be not only one. But content must be query one by one. That is like this:

`Query HostNameA HostNameB Content`
***
## DCClient
### The command list supported by MainThread
* Subscribe
* Query
* Exit

***
Just like server, the MainThread of client only responds to above command.
***
### The command list supported by SubscribeThread
* Subscribe
* Cancel
* Exit

***
The enter can be accept consists of *command* and *topic's name*, using the fixed order. Each input need to be separated by space.

`Subscribe Finance Military`

`Cancel Polity`
***
### The command list supported by Query
* Query
* LeaveWords (unimplements)

### The contents can be queried
* Topics
* HostNetwork

***
The whole command for query should at least consists of three parts. They are command of query, target **hosts'** name and query content, separated by single space. As emphasized before, counts of host can be not only one. But content must be query one by one. That is like this:

`Query HostNameA HostNameB Content`
***
## Tips:
* The initial of all command is **capital**, while others are **lowercase**.
* Single space is the **only** official separator.
* 'Exit' command is the quit command of **all** thread.