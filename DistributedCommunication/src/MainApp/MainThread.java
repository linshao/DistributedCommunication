package MainApp;

import java.util.*;

import DataCenter.ConfigAccessor;
import Service.ServiceThread;

public class MainThread extends Thread {

	private static final String TAG = "Server has been quited.";
	private static final String COMMAND_ERROR = "Error Command : ";
	private static final String INPUT_HINT = "Please enter the command:";

	private Scanner input;
	
	private HashSet<String> commands;

	public ServiceThread blockingThread;

	private boolean ifStopThread;
	
	public MainThread() {
		this.input = new Scanner(System.in);
		ifStopThread = true;
		this.commands = ConfigAccessor.sharedInstance().getCommandSet();
	}
	
	@Override
	public void run() {
		while (ifStopThread) {
			String inputContent = getInputContent();
			if (isInputContentCommand(inputContent))
				handleCommand(inputContent);
			else
				System.out.println(COMMAND_ERROR + inputContent);
		}
		
		System.out.println(TAG);
	}
	
	private String getInputContent() {
		System.out.println(INPUT_HINT);
		String inputContent = input.nextLine();
		return inputContent;
}
	
	private boolean isInputContentCommand(String inputContent) {
		return this.commands.contains(inputContent);
	}
	
	private void handleCommand(String command) {
		if (command.equals("Exit"))
			ifStopThread = false;
		else {
			String className = "ServiceThread." + command + "Thread";
			this.blockingThread = createThreadFromClassName(className);
			activeThreadAndBlockSelfThread(this.blockingThread);
		}
	}
	
	private ServiceThread createThreadFromClassName(String className) {
		ServiceThread sThread;
		
		try {
			sThread = (ServiceThread) Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			sThread = null;
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			sThread = null;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			sThread = null;
			e.printStackTrace();
		}
		return sThread;
	}

	private void activeThreadAndBlockSelfThread(ServiceThread blockingThread) {
		new Thread(blockingThread).start();
		
		synchronized(blockingThread) {
			try {
				blockingThread.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
