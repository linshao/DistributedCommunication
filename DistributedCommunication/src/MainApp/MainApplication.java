package MainApp;

import DataCenter.ConfigModifier;
import DataCenter.HostModifier;
import ServiceThread.*;

public class MainApplication {
	
	public static void main(String[] args) {
		
		String name = args[0];
		String port = args[1];
		ConfigModifier.sharedInstance().updateSelfInfo(name, port, "On");
		HostModifier.sharedInstance().setHostUsing(name);
		
		LogOnThread logOn = new LogOnThread();
		Thread logOnThread = new Thread(logOn);
		logOnThread.start();
		
		MainThread mainThread = new MainThread();
		mainThread.start();

		ListenThread listenThread = new ListenThread();
		listenThread.start();
		
		LogOffThread logOff = new LogOffThread();
		Runtime.getRuntime().addShutdownHook(new Thread(logOff));
	}

}
