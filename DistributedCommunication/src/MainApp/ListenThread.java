package MainApp;

import java.util.Map;

import DataCenter.ConfigAccessor;
import ServiceThread.ListenBcThread;
import ServiceThread.ListenLogInfo;
import ServiceThread.ListenQueryThread;

public class ListenThread extends Thread {

	public ListenThread() {
		
	}

	@Override
	public void run() {
		super.run();
		
		Map<String, String> broadcastConfig = ConfigAccessor.sharedInstance().getBroadcastConfig();
		String port = broadcastConfig.get("port");
		String listenGroup = broadcastConfig.get("broadcastGroup");
		
		ListenBcThread listenBCThread = new ListenBcThread(port, listenGroup);
		listenBCThread.start();
		
		ListenLogInfo log = new ListenLogInfo();
		Thread logInfo = new Thread(log);
		logInfo.start();
		
		ListenQueryThread listenQueryThread = new ListenQueryThread();
		listenQueryThread.start();
	}
}
