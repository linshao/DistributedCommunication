package DataCenter;

import java.util.HashSet;
import java.util.Map;

import DataBasic.MetaConfig;

public class ConfigAccessor extends MetaConfig{
	
	private static ConfigAccessor instance;

	private ConfigCenter configCenter;
	
	private HashSet<String> topics;
	private HashSet<String> commands;

	private Map<String, String> hostInfo;
	private Map<String, String> broadcastConfig;

	private HashSet<String> queryContents;
	private HashSet<String> queryCommands;

	private String name;

	public void setName(String name) {
		this.name = name;
	}

	public static synchronized ConfigAccessor sharedInstance() {
		if (instance == null)
			instance = new ConfigAccessor();
		return instance;
	}
	
	protected ConfigAccessor() {
		super();
		configCenter = ConfigCenter.sharedInstance();
	}
	
	public HashSet<String> getCommandSet() {
		if (commands == null) 
			this.commands = configCenter.generateSetValueForField("Commands");
		
		return commands;
	}
	
	public HashSet<String> getTopicNameSet() {
		if (topics == null) 
			this.topics = configCenter.generateSetValueForField("Topics");
		
		return topics;
	}
	
	public HashSet<String> getQueryCommands() {
		if (queryCommands == null)
			this.queryCommands = configCenter.generateSetValueForField("QueryCommands");
		
		return queryCommands;
	}
	
	public HashSet<String> getQueryContents() {
		if (queryContents == null)
			this.queryContents = configCenter.generateSetValueForField("QueryContents");
		
		return queryContents;
	}
	
	public Map<String, String> getBroadcastConfig() {
		if (this.broadcastConfig == null) {
			HashSet<String> keySet = new HashSet<String>(){
				{
					add("port");
					add("broadcastGroup");
				}
			};
			this.broadcastConfig = configCenter.generateMapForField(keySet);
		}
		
		return this.broadcastConfig;
	}
	
	public Map<String, String> getSelfInfo() {
		if (hostInfo == null) {
			HashSet<String> keySet = new HashSet<String>(){
				{
					add("Name");
					add("Ip");
					add("Port");
					add("State");
				}
			};
			hostInfo = configCenter.generateMapForField(keySet);
		}
		
		return hostInfo;
	}
	
	public String getHostName() {
		if (name == null)
			name = prop.getProperty("Name");
		return name;
	}
	
	/*
	 * print hold data
	 */

	public void printSupportedTopicNames() {
		String topicNames = "Supported Topics: ";
		for (String topicName : topics)
			topicNames = topicNames + topicName + " ";
		topicNames.trim();
		System.out.println(topicNames);
	}
	
}