package ServiceThread;

import java.util.Map;

import javax.jms.MapMessage;

import DataCenter.ConfigAccessor;
import DataCenter.ConfigModifier;
import ServiceProvider.LogMesSender;

public class LogOffThread implements Runnable {

	private static final String LOG = "Log";
	private LogMesSender logMesSender;

	public LogOffThread() {
		logMesSender = new LogMesSender(LOG);
	}

	@Override
	public void run() {
		ConfigModifier.sharedInstance().updateSelfState("Off");
		Map<String, String> hostInfo = ConfigAccessor.sharedInstance().getSelfInfo();
		MapMessage message = logMesSender.createMapMessage(hostInfo);
		logMesSender.sendMessage(message);
	}

}
