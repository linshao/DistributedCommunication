package ServiceThread;

import java.util.List;

import DataCenter.HostAccessor;
import ServiceProvider.*;

import Service.*;

public class QueryThread extends ServiceThread {
	
	private QueryCmdOperator queryCmdOperator;
	private QueryCmdParser queryCmdParser;
	
	public QueryThread() {
		super();
		
		this.cmdParser = new QueryCmdParser();
		this.queryCmdOperator = new QueryCmdOperator();
		
		queryCmdParser = (QueryCmdParser) cmdParser;
		
		INPUTHINT = "Enter the command, host names and querying content separated by space:";
		TAG = "Query service has been quited.";
	}

	@Override
	public void printHint() {
		super.printHint();
		HostAccessor.sharedInstance().printOnlineHosts();
	}

	@Override
	protected void handleBussinessCommand(String header, List<String> content) {
		super.handleBussinessCommand(header, content);
		
		List<String> hostList = queryCmdParser.parseContentForQueriedHostList(content);
		String queryContent = queryCmdParser.parseContentForQueryContent(content);
		
		if (queryCmdParser.isQuerySubCmdCorrect(hostList, queryContent))
			queryCmdOperator.queryHostForInfo(hostList, queryContent);
	}

}
