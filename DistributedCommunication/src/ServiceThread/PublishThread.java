package ServiceThread;

import java.util.*;

import DataCenter.ConfigAccessor;
import Service.ServiceThread;
import ServiceProvider.*;

public class PublishThread extends ServiceThread {

	private Publisher publisher;
	
	public PublishThread() {
		
		INPUTHINT = "Enter the topic and content separated by single space:(Quit with 'Exit')";
		CMDERRORHINT = "Error Command: ";
		TAG = "Exit publish";
		
		this.cmdParser = new PubCmdParser();
		this.publisher = new Publisher();
	}

	@Override
	protected void handleBussinessCommand(String header, List<String> content) {
		super.handleBussinessCommand(header, content);
		publisher.connectTopic(header);
		publisher.publishMessage(PubCmdParser.restoreSplitedContent(content));
	}

	@Override
	public void printHint() {
		super.printHint();
		ConfigAccessor.sharedInstance().printSupportedTopicNames();
	}
	
}
