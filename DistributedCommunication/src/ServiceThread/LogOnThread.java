package ServiceThread;

import java.util.Map;

import javax.jms.MapMessage;

import DataCenter.ConfigAccessor;
import ServiceProvider.LogMesSender;

public class LogOnThread implements Runnable {

	private static final String LOG = "Log";
	private LogMesSender logMesSender;

	public LogOnThread() {
		logMesSender = new LogMesSender(LOG);
	}

	@Override
	public void run() {
		Map<String, String> hostInfo = ConfigAccessor.sharedInstance().getSelfInfo();
		MapMessage message = logMesSender.createMapMessage(hostInfo);
		logMesSender.sendMessage(message);
	}

}
