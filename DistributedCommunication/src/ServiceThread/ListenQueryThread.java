package ServiceThread;

import ServiceProvider.*;
import java.util.*;
import java.io.IOException;
import java.net.*;

import DataCenter.ConfigAccessor;

public class ListenQueryThread extends Thread {
	
	private ServerSocket server;
	private String name;
	private boolean ifStopThread = true;

	public ListenQueryThread() {
		Map<String, String> networkConfig = ConfigAccessor.sharedInstance().getSelfInfo();
		name = networkConfig.get("Name");
		int port = Integer.parseInt(networkConfig.get("Port"));
		
		try {
			server = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		super.run();
		
		while (ifStopThread) {
			try {
				Socket socket = server.accept();
				new QueryHandler(name, socket).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
