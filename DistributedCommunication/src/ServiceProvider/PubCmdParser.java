package ServiceProvider;

import java.util.List;

import DataCenter.ConfigAccessor;
import Service.CmdParser;

public class PubCmdParser extends CmdParser{
	
	public PubCmdParser() {
		super();
		this.orglCmdSet = ConfigAccessor.sharedInstance().getTopicNameSet();
		this.exCmdSet = createExtendCmdSet(orglCmdSet);
	}
	
	public static String restoreSplitedContent(List<String> splitedInput) {
		String content = "";
		for (String input : splitedInput)
			content = content + input + " ";
		content.trim();
		return content;
	}
	
}
