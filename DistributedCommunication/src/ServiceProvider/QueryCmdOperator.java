package ServiceProvider;

import java.util.*;

import DataCenter.*;

public class QueryCmdOperator {

	private HostAccessor hostAccessor;
	
	public QueryCmdOperator() {
		hostAccessor = HostAccessor.sharedInstance();
		
	}
	
	public void queryHostForInfo(List<String> hosts, String queryContent) {
		for (String host : hosts) {
			String ip = hostAccessor.getTheIpOfHost(host);
			String port = hostAccessor.getThePortOfHost(host);
			QuerySender querySender = new QuerySender(ip, port);
			querySender.sendQueryContent(queryContent);
			querySender.start();
			HostModifier.sharedInstance().setHostUsing(host);
		}
	}
	
	public void leaveWordsOnMessageBoard(String words) {
		
	}
	
}
