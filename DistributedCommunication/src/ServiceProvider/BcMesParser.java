package ServiceProvider;


import java.util.*;

public class BcMesParser {

	public enum MessageType {
		HOSTSTATE, CHAT, 
	}
	
	public BcMesParser() {
		
	}

	public MessageType parseMessageHeaderForType(String header) {
		if (header.equals("HostState"))
			return MessageType.HOSTSTATE;
		else
			return MessageType.CHAT;
	}
	
	public Map<String, String> parseMessage(String message) {
		String[] splitedMes = message.split(":");
		String key1 = "header";
		String key2 = "content";
		Map<String, String> parsedResult = new HashMap<String, String>();
		parsedResult.put(key1, splitedMes[0]);
		parsedResult.put(key2, splitedMes[1]);
		return parsedResult;
	}

	public Map<String, String> parseContentForHostInfos(String content) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
