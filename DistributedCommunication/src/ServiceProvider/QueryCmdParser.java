package ServiceProvider;

import java.util.*;

import DataCenter.ConfigAccessor;
import DataCenter.HostAccessor;

import Service.CmdParser;

public class QueryCmdParser extends CmdParser{

	private static final String HOSTERROR_NAME = "Error host name: ";
	private static final String HOSTERROR_OFFLINE = "Error offline host: ";
	private static final String QUERYERROR_CONTENT = "Error query content: ";
	
	private ConfigAccessor configAccessor;
	private HostAccessor hostAccessor;
	
	private List<String> hostNames;
	private HashSet<String> queryContents;
	
	public QueryCmdParser() {
		configAccessor = ConfigAccessor.sharedInstance();
		hostAccessor = HostAccessor.sharedInstance();
		
		orglCmdSet = configAccessor.getQueryCommands();
		queryContents = configAccessor.getQueryContents();
		hostNames = hostAccessor.getHostNames();
		
		exCmdSet = createExtendCmdSet(orglCmdSet);
	}
	
	@Override
	protected boolean isTheBussinessCmdCorrect(String[] inputArray, String header) {
		return orglCmdSet.contains(header) && inputArray.length < 3;
	}

	public List<String> parseContentForQueriedHostList(List<String> content) {
		List<String> subContent = content.subList(0, content.size()-1);
		List<String> hostList = new ArrayList<String>();
		
		for (String host : subContent) {
			if (hostNames.contains(host)) {
				if (hostAccessor.isTheHostOnline(host) && hostAccessor.isTheHostUnusing(host))
					hostList.add(host);
				else
					System.out.println(HOSTERROR_OFFLINE + host);
			}
			else
				System.out.println(HOSTERROR_NAME + host);
		}
		
		return hostList;
	}
	
	public String parseContentForQueryContent(List<String> content) {
		String queryContent = content.get(content.size()-1);
		if (queryContents.contains(queryContent))
			return queryContent;
		else {
			System.out.println(QUERYERROR_CONTENT + queryContent);
			return null;
		}
			
	}
	
	public String restoreContentToString(List<String> content) {
		String orglContent = "";
		for (String Content : content)
			orglContent += Content + " ";
		
		return orglContent.trim();
	}
	
	public boolean isQuerySubCmdCorrect(List<String> hosts, String queryContent) {
		if (hosts.isEmpty() || hosts == null || queryContent == null)
			return false;
		else
			return true;
	}
	
}
