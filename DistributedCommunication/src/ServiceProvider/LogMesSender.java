package ServiceProvider;

import java.util.Map;
import javax.jms.*;

import JMSBasic.JMSBasicConfig;

public class LogMesSender extends JMSBasicConfig {

	private TopicSession session;
	private TopicPublisher publisher;

	public LogMesSender(String topicName) {
		super();
		try {
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic topic = (Topic) ctx.lookup(topicName);
			publisher = session.createPublisher(topic);
			connection.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public MapMessage createMapMessage(Map<String, String> mapData) {
		try {
			MapMessage mapMessage = session.createMapMessage();
			for (String key : mapData.keySet())
				mapMessage.setString(key, mapData.get(key));
			return mapMessage;
		} catch (JMSException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void sendMessage(Message message) {
		try {
			publisher.publish(message);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
