package DataBasic;

public class MetaHost extends MetaCenter {

	protected MetaHost() {
		super();
		
		FILE_NAME = "Host.properties";
		FILE_PATH = this.getClass().getClassLoader().getResource(FILE_NAME).getPath();
		this.prop = getPropertiesFile();
	}

}
