package DataBasic;

public class MetaConfig extends MetaCenter {

	protected MetaConfig() {
		super();
		
		FILE_NAME = "Config.properties";
		FILE_PATH = this.getClass().getClassLoader().getResource(FILE_NAME).getPath();
		this.prop = getPropertiesFile();
	}

}
