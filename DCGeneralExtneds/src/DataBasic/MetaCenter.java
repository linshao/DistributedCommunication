package DataBasic;

import java.io.InputStream;
import java.util.Properties;

public class MetaCenter {
	
	protected Properties prop;
	
	protected String FILE_PATH;
	protected String FILE_NAME;

	protected MetaCenter() {
		super();
	}

	protected Properties getPropertiesFile() {
		try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(FILE_NAME);
			Properties prop = new Properties();
			prop.load(inputStream);
			return prop;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}