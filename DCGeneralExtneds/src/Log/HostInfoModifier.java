package Log;

import javax.jms.MapMessage;

public interface HostInfoModifier {

	void updateHostInfo(MapMessage infos);
}
