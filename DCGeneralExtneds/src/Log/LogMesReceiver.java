package Log;

import javax.jms.*;

public class LogMesReceiver implements MessageListener {

	private HostInfoModifier hostInfoModifier;

	public LogMesReceiver(HostInfoModifier hiModifier) {
		hostInfoModifier = hiModifier;
	}

	@Override
	public void onMessage(Message message) {
		MapMessage infos = (MapMessage) message;
		hostInfoModifier.updateHostInfo(infos);
	}

}
