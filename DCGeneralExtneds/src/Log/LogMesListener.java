package Log;

import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import JMSBasic.JMSBasicConfig;

public class LogMesListener extends JMSBasicConfig{

	private TopicSubscriber subscriber;
	private Session session;

	public LogMesListener(String topicName, String subscriberName) {
		super();
		
		try {
			connection.setClientID(subscriberName);
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic topic = (Topic) ctx.lookup(topicName);
			subscriber = session.createDurableSubscriber(topic, subscriberName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setMessageReceiver(MessageListener receiver) {
		try {
			subscriber.setMessageListener(receiver);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
