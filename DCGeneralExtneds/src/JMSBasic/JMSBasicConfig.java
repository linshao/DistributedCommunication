package JMSBasic;

import javax.jms.JMSException;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.naming.InitialContext;

public class JMSBasicConfig {

	private static final String CONFACTORY_NAME = "TopicCF";
	protected InitialContext ctx;
	protected TopicConnection connection;

	public JMSBasicConfig() {
		try {
			ctx = new InitialContext();
			TopicConnectionFactory conFac = (TopicConnectionFactory)ctx.lookup(CONFACTORY_NAME);
			connection = conFac.createTopicConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void startConnection() {
		try {
			connection.start();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	public void endConnection() {
		try {
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
}
