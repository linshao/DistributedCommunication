package Service;

import java.util.*;

public class CmdParser {

	protected HashSet<String> orglCmdSet;
	protected HashSet<String> exCmdSet;

	public CmdParser() {
		
	}
	
	protected static HashSet<String> createExtendCmdSet(HashSet<String> OrglCmdSet) {
		HashSet<String> ExCmdSet = (HashSet<String>) OrglCmdSet.clone();
		ExCmdSet.add("Exit");
		return ExCmdSet;
	}
	
	public Map<String, Object> parseInputContent(String inputContent) {
		Map<String, Object> parseResult = new HashMap<String, Object>();
		String[] inputArray = inputContent.split(" ");
		
		String header = inputArray[0];
		List<String> content = new ArrayList(Arrays.asList(inputArray));
		content.remove(0);
		
		parseResult.put("header", header);
		parseResult.put("content", content);
		
		return parseResult;
	}
	
	public boolean isTheInputCorrectCommand(String inputContent) {
		String[] inputArray = inputContent.split(" ");
		String header = inputArray[0];
		
		if (exCmdSet.contains(header)) {
			if (isTheBussinessCmdCorrect(inputArray, header))
				return false; //if header isn't 'Exit', it must be followed by content
			return true;
		}
		else
			return false;
	}

	protected boolean isTheBussinessCmdCorrect(String[] inputArray, String header) {
		return orglCmdSet.contains(header) && inputArray.length < 2;
	}
	
	public boolean isHeaderTheExitCommand(String header) {
		if (header.equals("Exit"))
			return true;
		else
			return false;
	}

}
